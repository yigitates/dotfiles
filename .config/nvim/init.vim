" Disable cursor styling
set guicursor=

" Restore cursor position
autocmd BufRead * autocmd FileType <buffer> ++once
  \ if &ft !~# 'commit\|rebase' && line("'\"") > 1 && line("'\"") <= line("$") | exe 'normal! g`"' | endif

set title
set number relativenumber
