PROMPT='%F{blue}%~%f '

autoload -Uz compinit
compinit
zstyle ':completion:*' menu select

bindkey -v

HISTFILE="$XDG_CACHE_HOME/zsh/history"
SAVEHIST=1000
HISTSIZE=1000

alias bc='bc -lq'
alias cp='cp -iv'
alias grep='grep -i --color=auto'
alias ls='ls -h --color=auto --group-directories-first'
alias man='man -O width=184'
alias mkdir='mkdir -pv'
alias mv='mv -iv'
alias radeontop='radeontop -c'
alias reboot='sudo reboot'
alias rm='rm -vI'
alias shutdown='sudo shutdown -h now'
alias startx='startx "$XDG_CONFIG_HOME/x11/xinitrc"'

alias xinstall='sudo xbps-install -S'
alias xremove='sudo xbps-remove -R'
alias xsearch='xbps-query -Rs'
alias xupdate='sudo xbps-install -Su'

alias v=nvim

alias mcell='simple-mtpfs "$HOME/dl/cell"'
alias ucell='fusermount -u "$HOME/dl/cell"'

alias nsxivr="$XDG_CONFIG_HOME/nsxiv/exec/nsxiv-rifle"

alias weather='curl wttr.in'

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
