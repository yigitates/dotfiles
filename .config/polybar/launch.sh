#!/bin/bash

pkill polybar

echo "---" | tee -a /tmp/polybar.log
polybar mybar 2>&1 | tee -a /tmp/polybar.log & disown

echo "Polybar launched..."
