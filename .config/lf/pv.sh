#!/bin/sh

case "$1" in
  *.jpg|*.png|*.mp4) mediainfo "$1";;
  *) less -R "$1";;
esac
